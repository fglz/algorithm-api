from functools import reduce


def fibonacci(n):
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


def ackermann(n, m):
    if m == 0:
        return n + 1
    if n == 0:
        return ackermann(m - 1, 1)
    return ackermann(m - 1, ackermann(m, n - 1))


def factorial(n):
    return reduce(lambda x, y: x * y, range(1, n + 1))


algorithm_mapping = {
    "fibonacci": fibonacci,
    "ackermann": ackermann,
    "factorial": factorial,
}
