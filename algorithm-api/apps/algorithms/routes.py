from rest_framework import routers

from .views import AckermannViewSet
from .views import FactorialViewSet
from .views import FibonacciViewSet

app_name = "algorithms"

routes = routers.DefaultRouter()
routes.register(r"fibonacci", FibonacciViewSet, basename="fibonacci")
routes.register(r"ackermann", AckermannViewSet, basename="ackermann")
routes.register(r"factorial", FactorialViewSet, basename="factorial")

urlpatterns = routes.urls
