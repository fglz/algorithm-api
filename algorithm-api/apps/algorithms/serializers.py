from rest_framework import serializers


class FibonacciInputSerializer(serializers.Serializer):
    n = serializers.IntegerField(required=True, min_value=0)


class AckermannInputSerializer(serializers.Serializer):
    n = serializers.IntegerField(required=True, min_value=0)
    m = serializers.IntegerField(required=True, min_value=0)


class FactorialInputSerializer(serializers.Serializer):
    n = serializers.IntegerField(required=True, min_value=0)


class ResultSerializer(serializers.Serializer):
    processing_time = serializers.DecimalField(
        decimal_places=5, max_digits=10, coerce_to_string=False
    )
    result = serializers.IntegerField()
