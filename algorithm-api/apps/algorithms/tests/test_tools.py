import pytest

from ..algorithms import ackermann
from ..algorithms import factorial
from ..algorithms import fibonacci


@pytest.mark.parametrize(
    "input_n, expected_result",
    (
        (0, 0),
        (1, 1),
        (2, 1),
        (3, 2),
        (4, 3),
        (5, 5),
        (6, 8),
        (7, 13),
        (8, 21),
        (9, 34),
        (10, 55),
        (11, 89),
        (12, 144),
        (13, 233),
        (14, 377),
        (15, 610),
        (16, 987),
        (17, 1597),
        (18, 2584),
        (19, 4181),
        (20, 6765),
    ),
)
def test_fibonacci(input_n, expected_result):
    result = fibonacci(input_n)
    assert result == expected_result


@pytest.mark.parametrize(
    "input_n, expected_result",
    (
        (1, 1),
        (2, 2),
        (3, 6),
        (4, 24),
        (5, 120),
        (6, 720),
        (7, 5040),
        (8, 40320),
        (9, 362880),
        (10, 3628800),
        (11, 39916800),
        (12, 479001600),
        (13, 6227020800),
        (14, 87178291200),
        (15, 1307674368000),
        (16, 20922789888000),
        (17, 355687428096000),
        (18, 6402373705728000),
        (19, 121645100408832000),
        (20, 2432902008176640000),
    ),
)
def test_factorial(input_n, expected_result):
    result = factorial(input_n)
    assert result == expected_result


#  Test cases kept to a minimum due to recursive complexity
@pytest.mark.parametrize(
    "input_n, input_m, expected_result",
    ((0, 0, 1), (1, 0, 2), (2, 0, 3), (3, 0, 4), (4, 0, 5)),
)
def test_ackermann(input_n, input_m, expected_result):
    result = ackermann(input_n, input_m)
    assert result == expected_result
