import pytest
from rest_framework.test import APIClient


@pytest.fixture
def test_client():
    test_client = APIClient()
    return test_client
