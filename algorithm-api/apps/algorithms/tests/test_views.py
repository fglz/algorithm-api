from rest_framework import status


def test_fibonacci_without_inputs(test_client):
    url = "/algorithms/fibonacci/"
    payload = {}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["n"][0] == "This field is required."


def test_fibonacci(test_client):
    url = "/algorithms/fibonacci/"
    payload = {"n": 20}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_200_OK
    assert "processing_time" in response.json().keys()
    assert response.json()["result"] == 6765


def test_fibonacci_with_negative_input(test_client):
    url = "/algorithms/fibonacci/"
    payload = {"n": -1}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["n"][0] == "Ensure this value is greater than or equal to 0."


def test_ackermann_without_inputs(test_client):
    url = "/algorithms/ackermann/"
    payload = {}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["n"][0] == "This field is required."
    assert response.json()["m"][0] == "This field is required."


def test_ackermann(test_client):
    url = "/algorithms/ackermann/"
    payload = {"n": 1, "m": 0}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_200_OK
    assert "processing_time" in response.json().keys()
    assert response.json()["result"] == 2


def test_ackermann_with_negative_input(test_client):
    url = "/algorithms/ackermann/"
    payload = {"n": -1, "m": -1}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["n"][0] == "Ensure this value is greater than or equal to 0."
    assert response.json()["m"][0] == "Ensure this value is greater than or equal to 0."


def test_factorial_without_inputs(test_client):
    url = "/algorithms/factorial/"
    payload = {}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["n"][0] == "This field is required."


def test_factorial_with_negative_input(test_client):
    url = "/algorithms/factorial/"
    payload = {"n": -1}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["n"][0] == "Ensure this value is greater than or equal to 0."


def test_factorial(test_client):
    url = "/algorithms/factorial/"
    payload = {"n": 20}
    response = test_client.post(url, payload, format="json")
    assert response.status_code == status.HTTP_200_OK
    assert "processing_time" in response.json().keys()
    assert response.json()["result"] == 2432902008176640000
