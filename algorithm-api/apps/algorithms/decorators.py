import cProfile


def algorithm_profiling(func):
    def wrapper(*args, **kwargs):
        with cProfile.Profile() as pr:
            response = func(*args, **kwargs)
        pr.print_stats()
        return response

    return wrapper
