import time

from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from .algorithms import algorithm_mapping
from .decorators import algorithm_profiling
from .serializers import AckermannInputSerializer
from .serializers import FactorialInputSerializer
from .serializers import FibonacciInputSerializer
from .serializers import ResultSerializer


class BaseAlgorithmViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    algorithm = None

    @swagger_auto_schema(responses={200: ResultSerializer()})
    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        result = self.run_algorithm(**serializer.data)
        return Response(result.data, status=status.HTTP_200_OK)

    @algorithm_profiling
    def run_algorithm(self, **kwargs):
        start = time.time()
        result = algorithm_mapping[self.algorithm](**kwargs)
        delta = time.time() - start
        return ResultSerializer({"processing_time": delta, "result": result})


class FibonacciViewSet(BaseAlgorithmViewSet):
    serializer_class = FibonacciInputSerializer
    algorithm = "fibonacci"


class AckermannViewSet(BaseAlgorithmViewSet):
    serializer_class = AckermannInputSerializer
    algorithm = "ackermann"


class FactorialViewSet(BaseAlgorithmViewSet):
    serializer_class = FactorialInputSerializer
    algorithm = "factorial"
