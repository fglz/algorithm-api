FROM python:3.8

WORKDIR /home/app

RUN pip install poetry
COPY pyproject.toml poetry.lock /home/app/
RUN poetry config virtualenvs.create false
RUN poetry install --no-root

COPY . /home/app

RUN python algorithm-api/manage.py collectstatic
CMD python algorithm-api/manage.py runserver 0.0.0.0:8000