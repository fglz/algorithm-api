lint:
	pre-commit run -a -v

test:
	pytest -sx algorithm-api

runserver:
	python algorithm-api/manage.py runserver 0.0.0.0:8000

docker-test:
	docker run --rm --env-file .env -v `pwd`:/home/app/ --entrypoint pytest algorithm-api -sx algorithm-api

docker-build:
	docker build -t "algorithm-api" .

no_cache_build:
	docker build --no-cache -t "algorithm-api" .

docker-run:
	docker run -it -p 8000:8000 "algorithm-api" python algorithm-api/manage.py runserver 0.0.0.0:8000
