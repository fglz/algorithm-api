# Algorithm API

*Algorithm API* is an api that serves solutions and monitoring on famous algorithms like Ackermann, Fibonacci and Factorial.


Installing
----------

The project can either be run locally or with Docker in build container

### Docker

Running the project with docker is very straightforward:

- Make sure you have docker installed (https://docs.docker.com/get-docker/)
- Build the docker container by running:

.. code:: bash

    make docker-build

- Run the container by running:

.. code:: bash

    make docker-run

- Access the page http://0.0.0.0:8000/ to see the available endpoints

### Locally

Running the project locally is also very simple:

- Install requirements using pip:

.. code:: bash

    pip install -r requirements.txt

- Or poetry (https://python-poetry.org/)

.. code:: bash

    poetry install
    poetry shell #Starts virtual environment

- Run the server:

.. code:: bash

    make runserver

- Access the page http://0.0.0.0:8000/ to see the available endpoints


Tests
-------

#### Docker
.. code:: bash

    make docker-test
#### Locally
.. code:: bash

    make test

Profiling
-------
This project displays detailed information on each algorithm's performance using 
Python Profiling (https://docs.python.org/3/library/profile.html)
 
The result of the Profiling is shown on the command prompt on successful API calls, 
either by running the tests or manually making API requests
